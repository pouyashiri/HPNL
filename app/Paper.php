<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
    //
    public function Fields(){
        return $this->belongsToMany('\App\Field','paper_field');
    }
    public function Students(){
        return $this->belongsToMany('\App\Student','student_paper');
    }
    public function FieldRelations(){
        return $this->hasMany('\App\PaperField');
    }
    public function StudentRelations(){
        return $this->hasMany('\App\StudentPaper');
    }

}
