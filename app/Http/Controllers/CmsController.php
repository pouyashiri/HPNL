<?php

namespace App\Http\Controllers;

use App\StudentField;
use Illuminate\Http\Request;
use Validator;


class CmsController extends Controller
{

    public function __construct()
    {
        return $this->middleware(['auth', 'isadmin']);
    }


    //region STUDENT
    public function StudentShow($id)
    {
        $student = new \App\Student();
        $stdfields = NULL;
        $levels = \App\Level::all();
        $fields = \App\Field::all();
        if ($id != 0) {
            $student = \App\Student::find($id);
            $stdfields = $student->Fields;
        }
        return view('cms.newstudent', compact('levels', 'fields', 'student','stdfields'));
    }

    public function NewStudentPost(Request $req)
    {


        $this->validate($req, [
            'name' => 'required',
            'about' => 'required',
            'imagepath' => 'required',
            'resumepath' => 'required',
            'field_list' => 'required'
        ]);

        $f = new \App\Student();
        if ($req->new != 1)
            $f = \App\Student::find($req->id);
        $f->name = $req->name;
        $f->email = $req->email;

        $active = $req->input('isactive');
        $f->isactive = ($active == "on" ? true : false);
        $f->about = $req->about;
        $f->imagepath = $req->imagepath;
        $f->resumepath = $req->resumepath;
        $f->level_id = $req->level_id;
        $f->save();


        foreach ($f->FieldRelations as $fld) {
            $fld->delete();
        }
        foreach ($req->field_list as $fld) {
                $relation = new \App\StudentField();
                $relation->student_id = $f->id;
                $relation->field_id = $fld;
                $relation->save();
        }
        return redirect('/cms/students/all');
    }

    public function DeleteStudent($id)
    {

        $std = \App\Student::find($id);
        $fldRelsToDelete = $std->FieldRelations;
        foreach($fldRelsToDelete as $item)
            $item->delete();

        $pprRelsToDelete = $std->PaperRelations;
        foreach($pprRelsToDelete as $item)
            $item->delete();


        $std->delete();
        return redirect()->back();
    }

    public function EditStudent($id)
    {
        $student = \App\Student::find($id);
        return view('/cms/student/' + $id, compact('student'));
    }

    public function AllStudents()
    {
        $students = \App\Student::all();
        return view('cms.students', compact('students'));
    }
    //endregion


    //region PAPER
    public function PaperShow($id)
    {
        $pprfields=NULL;
        $pprstudents=NULL;
        $paper = new \App\Paper();
        if ($id != 0) {
            $paper = \App\Paper::find($id);
            $pprfields = $paper->Fields;
            $pprstudents = $paper->Students;
        }
        $students = \App\Student::all();
        $fields = \App\Field::all();
        return view('cms.newpaper', compact('students', 'fields', 'paper','pprfields','pprstudents'));
    }

    public function NewPaperPost(Request $req)
    {

//dd($req);
        $this->validate($req, [
            'author' => 'required',
            'title' => 'required',
            'field_list' => 'required',
            'student_list' => 'required'
        ]);

        $pprfields=NULL;
        $pprstudents=NULL;
        $f = new \App\Paper();
        if ($req->new != 1){
            $f = \App\Paper::find($req->id);
            $pprfields=$f->Fields;
            $pprstudents=$f->Students;
        }


        $f->author = $req->author;
        $f->title = $req->title;
        $f->year = $req->year;
        $f->journal = $req->journal;
        $f->volume = $req->volume;
        $f->number = $req->number;
        $f->pages = $req->pages;
        $f->doi = $req->doi;
        $f->keywords = $req->keywords;
        $f->booktitle = $req->booktitle;
        $f->location = $req->location;
        $f->address = $req->address;
        $f->bibtex = $req->input('bibtex');


        $f->save();

        foreach ($f->FieldRelations as $fld) {
            $fld->delete();
        }

        foreach ($f->StudentRelations as $std) {
            $std->delete();
        }

        foreach ($req->field_list as $fld) {
            $relation = new \App\PaperField();
            $relation->paper_id = $f->id;
            $relation->field_id = $fld;
            $relation->save();
        }

        foreach ($req->student_list as $std) {
            $relation = new \App\StudentPaper();
            $relation->paper_id = $f->id;
            $relation->student_id = $std;
            $relation->save();
        }

        return redirect('/cms/papers/all');
    }

    public function DeletePaper($id)
    {


        $ppr = \App\Paper::find($id);
        $ppr->delete();

        $fldRelsToDelete = $ppr->FieldRelations;
        foreach($fldRelsToDelete as $item)
            $item->delete();

        $stdRelsToDelete = $ppr->StudentRelations;
        foreach($stdRelsToDelete as $item)
            $item->delete();



        return redirect()->back();
    }

    public function EditPaper($id)
    {
        $paper = \App\Paper::find($id);
        return view('/cms/paper/' + $id, compact('paper'));
    }

    public function AllPapers()
    {
        $papers = \App\Paper::all();
        return view('cms.papers', compact('papers'));
    }
    //endregion

    //region FIELD
    public function FieldShow($id)
    {
        $field = new \App\Field();
        if ($id != 0) {
            $field = \App\Field::find($id);
        }
        return view('cms.newfield', compact('field'));
    }

    public function NewFieldPost(Request $req)
    {


        $this->validate($req, [
            'name' => 'required',
            'description' => 'required',
            'imagepath' => 'required'
        ]);

        $f = new \App\Field();
        if ($req->new != 1)
            $f = \App\Field::find($req->id);

        $f->name = $req->name;
        $f->description = $req->description;
        $f->imagepath = $req->imagepath;

        $f->save();
        return redirect('/cms/fields/all');
    }

    public function DeleteField($id)
    {
        $ppr = \App\Field::find($id);
        $ppr->delete();
        return redirect()->back();
    }

    public function EditField($id)
    {
        $field = \App\Field::find($id);
        return view('/cms/field/' + $id, compact('field'));
    }

    public function AllFields()
    {
        $fields = \App\Field::all();
        return view('cms.fields', compact('fields'));
    }
    //endregion

}
