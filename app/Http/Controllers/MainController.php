<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    public function Home(){
        $fields = \App\Field::all();
        $head = \App\Student::where('level_id','7')->get();
        $phds = \App\Student::whereIn('level_id',['1','2'])->get();
        $mscs = \App\Student::whereIn('level_id',['3','4'])->get();
        $bscs = \App\Student::whereIn('level_id',['5','6'])->get();

        return view('home',compact('fields','head','phds','mscs','bscs'));
    }

}
