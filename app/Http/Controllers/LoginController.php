<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    //
    public function login(){
        //return Auth::user()->role->name;
        return view('login');
    }
//    public function register(){
//        return view('register');
//    }
    public function checkUser(Request $request){
        $email=$request['email'];
        $pass=$request['password'];
        if(Auth::attempt(['email'=>$email,'password'=>$pass])){
            return redirect('/cms/students/all');
        }
        else{
            $err='اطلاعات وارد شده نادرست است.';
            return view('login',compact(['err']));
        }
    }
}
