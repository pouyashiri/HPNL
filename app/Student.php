<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    protected $table = "students";
    public function Fields(){
        return $this->belongsToMany('\App\Field','student_field');
    }
    public function Papers(){
        return $this->belongsToMany('\App\Paper','student_paper');
    }
    public function FieldRelations(){
        return $this->hasMany('\App\StudentField');
    }
    public function PaperRelations(){
        return $this->hasMany('\App\StudentPaper');
    }


}
