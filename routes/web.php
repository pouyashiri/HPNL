<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Field;
Route::get('/', 'MainController@Home');


Route::get('/cms/students/all','CmsController@AllStudents');

Route::get('/cms/student/{id}','CmsController@StudentShow');

Route::post('/cms/student/new','CmsController@NewStudentPost');

Route::get('/cms/student/delete/{id}', 'CmsController@DeleteStudent');

Route::get('/cms/student/edit/{id}','CmsController@EditStudent');



Route::get('/cms/fields/all','CmsController@AllFields');

Route::get('/cms/field/{id}','CmsController@FieldShow');



Route::post('/cms/field/new','CmsController@NewFieldPost');

Route::get('/cms/field/delete/{id}', 'CmsController@DeleteField');

Route::get('/cms/field/edit/{id}','CmsController@EditField');


Route::get('/cms/papers/all','CmsController@AllPapers');

Route::get('/cms/paper/{id}','CmsController@PaperShow');

Route::post('/cms/paper/new','CmsController@NewPaperPost');

Route::get('/cms/paper/delete/{id}', 'CmsController@DeletePaper');

Route::get('/cms/paper/edit/{id}','CmsController@EditPaper');



Route::get('/login',['as' => 'login', 'uses' => 'LoginController@login']);

Route::get('/register',['as' => 'register','uses' => 'LoginController@register']);

Route::post('/login','LoginController@checkUser');
