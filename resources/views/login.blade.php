@include('layouts.cmsheader')
<body style="direction: ltr;">

<style>
    .loginContent{
        width:500px;
        margin:auto;
        text-align: center;
    }
    .panel-heading{
        font-size:2em;
        font-weight:bolder;
        background: #bdbdbd;
        padding:50px
    }
</style>
<div class="container" style="background: #f7f7f7">
    <div class="row">
        <div class="loginContent">
            <div class="panel panel-default">
                <div class="panel-heading">HPNL CMS Login</div>
                <div class="panel-body">
                    @if(!empty($err))
                        <div class="alert alert-danger">
                            {{ $err }}
                        </div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="ontrol-label">Email</label>

                            <div class="">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>

                            <div class="">
                                <input id="password" type="password" class="form-control" name="password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="">
                                <button type="submit" class="btn btn-primary">Login</button>
                                {{--<p class="pull-left">عضو نیستید؟ <a class="btn btn-success" href="/register">ثبت نام</a></p>--}}
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

</body>




