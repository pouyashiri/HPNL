@extends('layouts.master')

@section('title', 'High Performance Network Laboratory | Home')

{{--@section('sidebar')--}}
{{--@parent--}}

{{--<p>This is appended to the master sidebar.</p>--}}
{{--@endsection--}}

@section('content')

    <style type="text/css">

        .lsArea-desc {
            padding: 10px
        }

        .lsArea-name {
            text-align: center;
            font-size: 1.5em
        }

        .lsArea-pic {
        }

        .lsArea-pic img {
            width: 100%;
            height: 100%;
        }

        .badge {
            display: block;
            width: 175px;
            line-height: 20px;
        }

        @media only screen and (max-width: 576px) {
            .dvPersonInfo {
                padding: 0
            }

            .badge {
                margin: 10px auto;
                line-height: 14px;
            }

            .more-info {
                width: 100px;
                margin: auto;
            }

            .dvPersonName {
                font-size: 0.8em !important
            }

            .main-desc {
                padding-top: 70px;
                font-size: 1em !important
            }

            .dvPersonExp {
                line-height: 15px;
                font-size: 0.7em;
            }

            .dvMainName {
                font-size: 8px !important
            }

            .dvMainName img {
                width: 25px !important;
                height: 25px !important;
            }

            .dvSlide img {
                height: 180px !important
            }
        }

        .main-desc {
            padding-top: 70px;
            font-size: 1.2em
        }

        .dvContMain {
            text-align: left !important;
            padding: 0;
        }

        .dvMainName {
            padding: 10px;
            position: absolute;
            z-index: 100;
            background: rgba(255, 255, 255, 0.95);
            top: 30px;
            /* width: 1000px; */
            /*        box-shadow: 0 0 20px 5px;*/
            margin: auto;
            color: #002337;
        }

        .dvNewsItem {
            background: rgba(0, 0, 0, 0.03);
            padding: 10px;
        }

        .dvDr {
            border: 1px #dedede solid;
            padding: 10px;
            margin-bottom: 25px
        }

        /*.stdSlide .lslide{
            height:140px !important
        }*/

        .dvPrjExp {
            padding-left: 8px
        }

        .dvPrjItem {
            background: rgba(0, 0, 0, 0.03);
            padding: 10px;
            margin-bottom: 10px
        }

        .dvPrjName {
            /*        font-size: 16px;*/
            padding: 0 0 10px 10px;
        }

        .dvPrjName:before {
            font-family: FontAwesome;
            content: "\f00c"
        }

        .dvMainName img {
            width: 60px;
            height: 60px;
        }

        .dvLabEx {
            float: left;
            padding: 20px 0 0 10px;
            width: 280px;
            line-height: 25px;
            font-size: 15px;
            margin-bottom: 15px;
        }

        .dvLabExImg {
            float: left;
            width: 215px;
        }

        .dvLabExImg img {
            width: 100%;
            height: 100%
        }

        .dvPrjImg {
            float: left;

            border: 1px #d2d2d2 solid;
        }

        .dvPrjImg img {
            width: 100%;
            height: 100%;
        }

        .labimg {
            width: 400px;
            margin: auto;
            height: 100%;
            max-width: 100%;
            max-height: 310px;
            display: block;
        }
    </style>

    <div class="row mt-3">
        <div class="col-md">

            <div class="dvCont dvContMain">
                <div class="dvMainName hidden-xs">
                    <img src="{{asset('img/home/tehlogo.png')}}"/>
                    High Performance Network Laboratory - University of Tehran

                </div>

                <ul id="lightSlider">
                    <li>
                        <div class="dvSlide">
                            <img src="{{asset('img/home/1.jpg')}}"/>
                            <div class="dvSlideSub"></div>
                        </div>
                    </li>
                    <li>
                        <div class="dvSlide">
                            <img src="{{asset('img/home/2.jpg')}}"/>
                            <div class="dvSlideSub"></div>
                        </div>
                    </li>

                </ul>

            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-12 col-sm-6">
            <div class="dvCont" style="min-height: unset !important; height: 690px">
                <div class="dvContTitle">
                    <span class="fa fa-connectdevelop"></span>
                    Who are we?
                    <br/>
                    <hr/>
                </div>
                <div class="col nopad">

                    <img class="labimg" src="{{asset('img/home/about.png')}}"/>
                </div>
                <div class="w-100"></div>
                <div class="col main-desc">
                    A unique blend of fundamental and applied research team, taking advantage of mathematical theorems
                    so as
                    to build practical systems for improving performance, dependability and security of computer and
                    communication systems.
                </div>


            </div>

        </div>


        <div class="col-12 col-sm-6">
            <div class="dvCont" style="min-height: unset !important">
                <div class="dvContTitle">
                    <span class="fa fa-newspaper-o"></span>
                    Research Areas
                    <br/>
                    <hr/>
                </div>
                <ul id="lightSlider_areas">
                    @foreach ($fields as $field)
                        {
                        <li>
                            <div class="lsArea-pic">
                                <img class="labimg" src="{{$field->imagepath}}"/>
                            </div>
                            <div class="lsArea-name">
                                {{$field->name}}
                            </div>
                            <div class="lsArea-desc">
                                {!! $field->description !!}

                            </div>
                        </li>
                        }
                    @endforeach
                </ul>
                {{--                        @foreach (var item in Model.prjs)--}}
                {{--            {--}}
                {{--                <div class="dvPrjItem col-md-12">--}}
                {{--                    <div class="dvPrjImg col-md-3 col-xs-12 col-sm-12  nopad">--}}
                {{--                        <img src="@($"/Images/Projects/{item.ImageURL}")"/>--}}
                {{--                    </div>--}}
                {{--                    <div class="col-md-9 nopad" style="font-size: 14px">--}}
                {{--                        <div class="dvPrjName">--}}
                {{--                            @item.Name--}}
                {{--                        </div>--}}
                {{--                        <div class="dvPrjExp">--}}
                {{--                            @{ var desc = item.Description.Substring(0, Math.Min(item.Description.Length, 120)); }--}}
                {{--                            @($"{desc}...")--}}
                {{--                            <a href="@string.Format("/Projects/{0}", item.Name.Replace(" ", "_"))">[Read More...]</a>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {{--                --}}
                {{--            }--}}

            </div>
        </div>
    </div>

    {{--<div class="col-md-6">--}}
    {{--    <div class="dvCont" style="min-height: unset !important">--}}
    {{--        <div class="dvContTitle">--}}
    {{--            <span class="fa fa-newspaper-o"></span>--}}
    {{--            Lab News--}}
    {{--            <br/>--}}
    {{--            <hr/>--}}
    {{--        </div>--}}
    {{----}}
    {{--        <ul id="lightSlider_2">--}}
    {{--            @foreach (var item in Model.news)--}}
    {{--            {--}}
    {{--                <li>--}}
    {{--                    <div class="dvNewsItem">--}}
    {{--                        <span class="fa fa-check"></span>--}}
    {{--                        @item.Title--}}
    {{--                        @if (!string.IsNullOrEmpty(item.LinkURL))--}}
    {{--                        {--}}
    {{--                            <a href="@item.LinkURL">[More Info...]</a>--}}
    {{--                        }--}}
    {{--                    </div>--}}
    {{--                </li>--}}
    {{--            }--}}
    {{--        </ul>--}}
    {{----}}
    {{----}}
    {{--    </div>--}}
    {{--</div>--}}


    <div class="row">


        {{--<div class="col-12">--}}


        <div class="head-div col-12 col-sm-4">
            <div class="dvCont" style="    height: 360px;">
                <div class="head-badge badge-dr">
                    Head
                </div>
                <img src="{{$head[0]->imagepath}}" alt="">
                <div class="head-name">
                    {{$head[0]->name}}
                </div>
                <div class="head-fields">
                    @foreach($head[0]->Fields as $fld)
                        <div class="head-field">
                            {{$fld->name}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="head-desc col-12 col-sm-8">
            <div class="dvCont" style="">
                {!! $head[0]->about !!}
            </div>
        </div>

        {{--</div>--}}
        {{--</div>--}}
    </div>

    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            //$('.dvPersonExp').each(function () {
            //    $(this).html($(this).text());
            //});
//            alert(1);
            $("#lightSlider_areas").lightSlider({
                item: 1,
                autoWidth: false,
                slideMove: 1, // slidemove will be 1 if loop is true
                slideMargin: 10,

                addClass: '',
                mode: "slide",
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////

                speed: 2000, //ms'
                auto: true,
                loop: true,
                slideEndAnimation: true,
                pause: 6000,

                keyPress: false,
                controls: true,
                //prevHtml: '',
                //nextHtml: '',

                rtl: false,
                adaptiveHeight: false,

                vertical: false,
                verticalHeight: 500,
                vThumbWidth: 100,

                thumbItem: 10,
                pager: true,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',

                enableTouch: true,
                enableDrag: true,
                freeMove: true,
                swipeThreshold: 40,


            });


            $("#lightSlider").lightSlider({
                item: 1,
                autoWidth: false,
                slideMove: 1, // slidemove will be 1 if loop is true
                slideMargin: 10,

                addClass: '',
                mode: "slide",
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////

                speed: 2000, //ms'
                auto: true,
                loop: true,
                slideEndAnimation: true,
                pause: 6000,

                keyPress: false,
                controls: true,
                //prevHtml: '',
                //nextHtml: '',

                rtl: false,
                adaptiveHeight: false,

                vertical: false,
                verticalHeight: 500,
                vThumbWidth: 100,

                thumbItem: 10,
                pager: false,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',

                enableTouch: true,
                enableDrag: true,
                freeMove: true,
                swipeThreshold: 40,


            });
            $("#lightSlider_2").lightSlider({
                item: 6,
                autoWidth: false,
                slideMove: 1, // slidemove will be 1 if loop is true
                slideMargin: 10,

                addClass: '',
                mode: "slide",
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////

                speed: 2000, //ms'
                auto: true,
                loop: true,
                slideEndAnimation: true,
                pause: 6000,

                keyPress: false,
                controls: false,
                //prevHtml: '',
                //nextHtml: '',

                rtl: false,
                adaptiveHeight: true,

                vertical: true,
                verticalHeight: 300,
                vThumbWidth: 100,

                thumbItem: 0,
                pager: false,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',

                enableTouch: true,
                enableDrag: true,
                freeMove: true,
                swipeThreshold: 40,


            });
            $("#lightSlider_3").lightSlider({
                item: 2,
                autoWidth: false,
                slideMove: 1, // slidemove will be 1 if loop is true
                slideMargin: 10,

                addClass: 'stdSlide',
                mode: "slide",
                useCSS: true,
                cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
                easing: 'linear', //'for jquery animation',////

                speed: 800, //ms'
                auto: true,
                loop: true,
                slideEndAnimation: true,
                pause: 3000,

                keyPress: false,
                controls: false,
                //prevHtml: '',
                //nextHtml: '',

                rtl: false,
                adaptiveHeight: true,

                vertical: true,
                verticalHeight: 420,
                vThumbWidth: 100,
                pauseOnHover: true,

                thumbItem: 0,
                pager: false,
                gallery: false,
                galleryMargin: 5,
                thumbMargin: 5,
                currentPagerPosition: 'middle',

                enableTouch: true,
                enableDrag: true,
                freeMove: true,
                swipeThreshold: 40,
                responsive: [
//                {
//                    breakpoint: 800,
//                    settings: {
//                        item: 3,
//                        slideMove: 1,
//                        slideMargin: 6,
//                    }
//                },
                    {
                        breakpoint: 480,
                        settings: {
                            item: 1,
                            verticalHeight: 450,

                        }
                    }
                ]

            });


        });

    </script>
    <style>
        .head-div {
            display: block;
            /*background: white;*/
            text-align: center;
            padding: 20px;
        }

        .head-name {
            font-size: 1.2em
        }

        .head-field {

            display: block;
            background: whitesmoke;
            padding: 0 10px;

            font-size: 0.8em;
            max-width: 150px;
            margin: 10px auto;
            font-style: italic;
        }

        .head-badge {
            position: relative;
            top: 10px;
            left: -33px;
            width: 110px;
            text-align: center;
            color: #4e4e4e;
            border-radius: 10px;
            -webkit-transform: rotate(-45deg);
            -moz-transform: rotate(-45deg);
            -ms-transform: rotate(-45deg);
            -o-transform: rotate(-45deg);
            transform: rotate(-45deg);
        }

        .head-desc {
            padding: 20px;
            padding-left: 0;
        }
    </style>
@endsection
