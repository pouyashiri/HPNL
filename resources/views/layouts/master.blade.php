<html>
<head>
    <meta name="viewport" http-equiv="Content-Type" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description"
          content="High Performance Network Laboroatory is located in Tehran, Iran. It consits of a unique blend of fundamental and applied research team, taking advantage of mathematical theorems so as to build practical systems for improving performance, dependability and security of computer and communication systems.">

    <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">

    <title>@yield('title')</title>

    {{--<link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">--}}

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    {{--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"--}}
    {{--integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"--}}
    {{--crossorigin="anonymous"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>

    <script src="{{asset('js/lightslider.js')}}" type="application/javascript"></script>


    {{--    <link href="{{asset('css/navbar.css')}}" type="text/css"/>--}}
    {{--<link href="/css/lightslider.css" type="text/css"/>--}}
    <link rel="stylesheet" href="{{asset('/css/main.css')}}">

    <link rel="stylesheet" href="{{asset('/css/lightslider.css')}}">

</head>
<body>


<body>
<header>
    <!--<div class="dvHeadItem" onclick="alert(1)">Home</div>
    <div class="dvHeadItem" onclick="alert(1)"> Papers</div>
    <div class="dvHeadItem" onclick="alert(1)">Students </div>
    <div class="dvHeadItem" onclick="alert(1)"> Projects </div>
    <div class="dvHeadItem" onclick="alert(1)"> Projects </div>
    <div class="dvHeadItem" onclick="alert(1)"> Projects </div>
    <div class="dvHeadItem" onclick="alert(1)"> Projects </div>-->

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand d-none d-sm-none" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                </li>
                {{--<li class="nav-item">--}}
                {{--<a class="nav-link" href="#">Link</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item dropdown">--}}
                {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                {{--Dropdown--}}
                {{--</a>--}}
                {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                {{--<a class="dropdown-item" href="#">Action</a>--}}
                {{--<a class="dropdown-item" href="#">Another action</a>--}}
                {{--<div class="dropdown-divider"></div>--}}
                {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                {{--<a class="nav-link disabled" href="#">Disabled</a>--}}
                {{--</li>--}}
            </ul>
            {{--<form class="form-inline my-2 my-lg-0">--}}
            {{--<input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">--}}
            {{--<button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>--}}
            {{--</form>--}}
        </div>
    </nav>
</header>

<div class="container">

    <div class="body">
        {{--<div class="row" style="margin-top: 10px">--}}
        @yield('content')
        {{--</div>--}}


    </div>


</div>

<div class="footer">This is the Footer Section</div>

</body>


</body>
</html>