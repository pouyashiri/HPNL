@include('layouts.cmsheader')
<body style="direction: ltr;">
<style>
    .leftMenu a {
        display: block;
        padding: 10px;
    }

    .leftMenu a.selected {
        background: #e9f9ff;
    }

    .leftMenu {
        padding: 20px;
        background: #fbfbfb;
    }

    .NAME {
        padding: 13px;
        background: #ececec;
        font-size: 1.5em;
    }
</style>
<div class="container" style="background: #f7f7f7">
    <div class="row">

        <div class="col-md-3 leftMenu">
            <div class="col-md-12">
                <div class="NAME">
                    HPNL CMS
                </div>
                <a href="/cms/student/new">New Student </a>
                <a href="/cms/students/all">List of Students </a>
                <a href="/cms/paper/new">New Paper </a>
                <a href="/cms/papers/all"> List of Papers</a>
                <a href="/cms/field/new">New Field </a>
                <a href="/cms/fields/all"> List of Fields</a>
            </div>
        </div>
        <div class="col-md-9" style="padding:20px">
            @yield('content')
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("a[href*='" + location.pathname + "']").addClass('selected');
    });
</script>
</body>
{{--@include('layouts.footer')--}}
@yield('other_js')