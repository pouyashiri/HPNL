@extends('layouts.cmsmaster')

@section('content')
    <div class="row">
        <div class="col-md">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Edit</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($fields as $field)
                    <tr>
                        <td><img class='img-table' src="{{$field->imagepath}}" alt=""/></td>
                        <td>{{$field->name}}</td>
                        <td><a  href="/cms/field/{{$field->id}}" class="btn btn-warning fa fa-edit">Edit</a></td>
                        <td><a where="/cms/field/delete/{{$field->id}}" onclick="confirmandgo($(this).attr('where'));"  class="btn btn-danger fa fa-edit">Remove</a></td>

                    </tr>


                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style type="text/css">
        .img-table {
            width: 100px
        }

    </style>
    <script>
        function confirmandgo(url){
            var x = confirm('َAre you sure?');
            if(x)
                location.href=url;
        }
    </script>
@endsection



