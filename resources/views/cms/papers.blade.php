@extends('layouts.cmsmaster')

@section('content')
    <div class="row">
        <div class="col-md">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th>
                    <th>Year</th>
                    <th>Edit</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($papers as $paper)
                    <tr>
                        <td>{{$paper->title}}</td>
                        <td>{{$paper->author}}</td>
                        <td>{{$paper->year}}</td>
                        <td><a href="/cms/paper/{{$paper->id}}" class="btn btn-warning fa fa-edit">Edit</a></td>
                        <td><a where="/cms/paper/delete/{{$paper->id}}" onclick="confirmandgo($(this).attr('where'));"
                               class="btn btn-danger fa fa-edit">Remove</a></td>
                    </tr>


                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style type="text/css">
        .img-table {
            width: 100px
        }

    </style>
    <script>
        function confirmandgo(url) {
            var x = confirm('َAre you sure?');
            if (x)
                location.href = url;
        }
    </script>
@endsection



