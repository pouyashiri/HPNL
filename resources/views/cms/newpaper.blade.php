@extends('layouts.cmsmaster')

@section('content')
    <div class="row">
        <div class="col-md">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal ui form" role="form" method="POST"
                  action="{{ url('/cms/paper/new') }}"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <fieldset>

                    <input style="display: none;" name="new" value="{{$paper->title == NULL ? 1 : 0}}"/>
                    <input style="display: none;" name="id" value="{{$paper->title == NULL ? 0 : $paper->id}}"/>
                    <!-- Form Name -->
                    <legend>
                        @if($paper->name == NULL)
                            New Paper
                        @else
                            Edit Paper
                        @endif

                    </legend>

                    <div class="form-group">
                        <label class="col-md control-label" for="abouttext">Bibtex</label>
                        <div class="col-md">
                            <textarea class="form-control input-md" name="bibtex" id="bibtext" cols="30"
                                      rows="10">{{$paper->bibtex}}</textarea>
                        </div>
                    </div>
                    <input type="text" name="bibtext" style="display: none;">
                    <div class="form-group">
                        <label class="col-md control-label" for="slclevel">Fields</label>
                        <div class="col-md">
                            <select name="field_list[]" id="slcfields" class="ui selection dropdown"
                                    multiple="multiple">

                                @foreach($fields as $fld)


                                    @if($pprfields != NULL)
                                        <option value="{{$fld->id}}" {{$pprfields->contains($fld) ? "selected" : "" }} >{{$fld->name}}</option>
                                    @else
                                        <option value="{{$fld->id}}">{{$fld->name}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                    </div>




                    <div class="form-group">
                        <label class="col-md control-label" for="slclevel">Students</label>
                        <div class="col-md">
                            <select name="student_list[]" id="slcstudents" class="ui selection dropdown"
                                    multiple="multiple">

                                @foreach($students as $std)


                                    @if($pprstudents != NULL)
                                        <option value="{{$std->id}}" {{$pprstudents->contains($std) ? "selected" : "" }} >{{$std->name}}</option>
                                    @else
                                        <option value="{{$std->id}}">{{$std->name}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                    </div>




                    <div class="form-group">
                        <button type="button" id="" class="btn btn-success" onclick="convertAndSubmit()">Submit</button>
                    </div>

                    <input type="text" name="author" style="display: none;">
                    <input type="text" name="title" style="display: none;">
                    <input type="text" name="year" style="display: none;">
                    <input type="text" name="journal" style="display: none;">
                    <input type="text" name="volume" style="display: none;">
                    <input type="text" name="number" style="display: none;">
                    <input type="text" name="pages" style="display: none;">
                    <input type="text" name="doi" style="display: none;">
                    <input type="text" name="keywords" style="display: none;">
                    <input type="text" name="booktitle" style="display: none;">
                    <input type="text" name="location" style="display: none;">
                    <input type="text" name="address" style="display: none;">
                </fieldset>
            </form>
        </div>
    </div>

    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script src="/js/bibtexParse.js"></script>
    <script>
        $(document).ready(function(){
            $('#slcfields').dropdown();
            $('#slcstudents').dropdown();
        });

        function convertAndSubmit(){
            var temp = bibtexParse.toJSON($('#bibtext').val());
            var info = temp[0].entryTags;
            $("input[name*='author']").val(info.author);
            $("input[name*='title']").val(info.title);
            $("input[name*='year']").val(info.year);
            $("input[name*='journal']").val(info.journal);
            $("input[name*='volume']").val(info.volume);
            $("input[name*='pages']").val(info.pages);
            $("input[name*='doi']").val(info.doi);
            $("input[name*='keywords']").val(info.keywords);
            $("input[name*='booktitle']").val(info.booktitle);
            $("input[name*='location']").val(info.location);
            $("input[name*='address']").val(info.address);
            $('form').submit();
        };
    </script>
@endsection