@extends('layouts.cmsmaster')

@section('content')
    <div class="row">
        <div class="col-md">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" role="form" method="POST"
                  action="{{ url('/cms/field/new') }}"
                  enctype="multipart/form-data">
                {{ csrf_field() }}


                <fieldset>
                    <input style="display: none;" name="new" value="{{$field->name == NULL ? 1 : 0}}"/>
                    <input style="display: none;" name="id" value="{{$field->name == NULL ? 0 : $field->id}}"/>
                    <!-- Form Name -->
                    <legend>
                        @if($field->name == NULL)
                            New Field
                        @else
                            Edit Field
                        @endif
                    </legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md control-label" for="textinput">Name</label>
                        <div class="col-md">
                            <input id="textinput" name="name" type="text"
                                   class="form-control input-md" value="{{$field->name}}">
                            {{--<span class="help-block">help</span>--}}
                        </div>
                    </div>

                    <!-- input-->
                    <div class="form-group">
                        <label class="col-md control-label" for="passwordinput">Description</label>
                        <div class="col-md">
                            <textarea id="txtDesc" name="description"
                                      class="form-control input-md">{{$field->description}}</textarea>

                            {{--<span class="help-block">help</span>--}}
                        </div>
                    </div>

                    <!-- Bibtex input-->
                    <div class="form-group hidden">
                        <button type="button" id='lfm' data-input="imagepath" data-preview="holder"
                                class="btn btn-info">Upload Image
                        </button>

                        <input id="imagepath" name="imagepath" type="text"
                               class="form-control input-md" value="{{$field->imagepath}}">
                        {{--<span class="help-block">help</span>--}}

                    </div>
                    <div class="form-group">
                        <img id="holder" src="{{$field->imagepath}}" alt="" style="width:300px;">
                    </div>
                    <div class="form-group">

                        <button type="submit" href="#" class="btn btn-success">Submit</button>

                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>
    <script>
        $('#lfm').filemanager('image');
        tinymce.init({
            selector: '#txtDesc',
            directionality: 'ltr',
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor"
            ],
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code | ltr rtl "
        });
    </script>
@endsection