@extends('layouts.cmsmaster')

@section('content')
    <div class="row">
        <div class="col-md">
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Name</th>
                    <th>Edit</th>
                    <th>Remove</th>
                </tr>
                </thead>
                <tbody>
                @foreach($students as $std)
                    <tr>
                        <td><img class='img-table' src="{{$std->imagepath}}" alt=""/></td>
                        <td>{{$std->name}}</td>
                        <td><a href="/cms/student/{{$std->id}}" class="btn btn-warning fa fa-edit">Edit</a></td>
                        <td><a where="/cms/student/delete/{{$std->id}}" onclick="confirmandgo($(this).attr('where'));"  class="btn btn-danger fa fa-edit">Remove</a></td>

                    </tr>


                @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <style type="text/css">
        .img-table {
            width: 100px
        }

    </style>
    <script>
        function confirmandgo(url){
            var x = confirm('َAre you sure?');
            if(x)
                location.href=url;
        }
    </script>
@endsection



