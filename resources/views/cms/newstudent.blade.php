@extends('layouts.cmsmaster')

@section('content')
    <div class="row">
        <div class="col-md">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal ui form" role="form" method="POST"
                  action="{{ url('/cms/student/new') }}"
                  enctype="multipart/form-data">
                {{ csrf_field() }}
                <fieldset>

                    <input style="display: none;" name="new" value="{{$student->name == NULL ? 1 : 0}}"/>
                    <input style="display: none;" name="id" value="{{$student->name == NULL ? 0 : $student->id}}"/>
                    <!-- Form Name -->
                    <legend>
                        @if($student->name == NULL)
                            New Student
                        @else
                            Edit Student
                        @endif

                    </legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md control-label" for="textinput">Full Name</label>
                        <div class="col-md">
                            <input id="textinput" name="name" type="text"
                                   class="form-control input-md" value="{{$student->name}}">
                            {{--<span class="help-block">help</span>--}}
                        </div>
                    </div>

                    <!-- Email input-->
                    <div class="form-group">
                        <label class="col-md control-label" for="emailinput">Email</label>
                        <div class="col-md">
                            <input id="emailinput" name="email" type="email"
                                   class="form-control input-md" value="{{$student->email}}">
                            {{--<span class="help-block">help</span>--}}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md control-label" for="abouttext">About</label>
                        <div class="col-md">
                            <textarea class="form-control input-md" name="about" id="abouttext" cols="30"
                                      rows="10">{{$student->about}}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md control-label" for="slclevel">Level</label>
                        <div class="col-md">
                            <select name="level_id" id="slclevel" class="">

                                @foreach($levels as $lvl)
                                    <option value="{{$lvl->id}}" {{$student->level_id == $lvl->id ? "selected" : ""}}>{{$lvl->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    {{--                    {!!  Form::select('size', array('L' => 'Large', 'S' => 'Small')) !!}--}}


                    <div class="form-group">
                        <label class="col-md control-label" for="slclevel">Fields</label>
                        <div class="col-md">
                            <select name="field_list[]" id="slcfields" class="ui selection dropdown"
                                    multiple="multiple">

                                @foreach($fields as $fld)


                                    @if($stdfields != NULL)
                                        <option value="{{$fld->id}}" {{$stdfields->contains($fld) ? "selected" : "" }} >{{$fld->name}}</option>
                                    @else
                                        <option value="{{$fld->id}}">{{$fld->name}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label" for="chk">Active?</label>
                        <input id="chk" type="checkbox" name="isactive[]"
                               class="" {{$student->isactive ? "selected"  : ""}}>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-info" type="button" id="lfm_1" data-input="imagepath"
                                data-preview="imgholder">Upload Image
                        </button>
                        <input type="text" id="imagepath" name="imagepath" value="{{$student->imagepath}}">
                    </div>


                    <div class="form-group">
                        <button class="btn btn-info" type="button" id="lfm_2" data-input="resumepath" data-preview="">
                            Upload Resume
                        </button>
                        <input type="text" id="resumepath" name="resumepath" value="{{$student->resumepath}}">
                    </div>


                    <div class="form-group">
                        <img id="imgholder" src="{{$student->imagepath}}" alt="" style="width:100px">
                    </div>


                    <div class="form-group">
                        <button type="submit" id="" class="btn btn-success">Submit</button>
                    </div>


                </fieldset>
            </form>
        </div>
    </div>
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
    <script src="/vendor/laravel-filemanager/js/lfm.js"></script>

    <script>
        $('#slcfields').dropdown();
        $('#lfm_1').filemanager('image');
        $('#lfm_2').filemanager('file');

        tinymce.init({
            selector: '#abouttext',
            directionality: 'ltr',
            plugins: [
                "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor"
            ],
            toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code | ltr rtl "
        });
    </script>
@endsection