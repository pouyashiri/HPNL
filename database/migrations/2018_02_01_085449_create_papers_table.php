<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePapersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('papers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('bibtex');
            $table->string('author');
            $table->text('title');
            $table->string('year')->nullable();
            $table->string('journal')->nullable();
            $table->string('volume')->nullable();
            $table->string('number')->nullable();
            $table->string('pages')->nullable();
            $table->string('doi')->nullable();
            $table->string('keywords')->nullable();
            $table->string('booktitle')->nullable();
            $table->string('location')->nullable();
            $table->string('address')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('papers');
    }
}
